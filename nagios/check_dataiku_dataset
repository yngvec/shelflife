#!/bin/bash
##################################################################
#
# Nagios check plugin -
#
# Check status for DataIku DSS Project datasets
#
##################################################################
#
# Requires curl and jq installed
#
##################################################################
#
# By :
#    Yngve L Clausen - Jan 2019++
#
##################################################################
#
# Notes / TODO :
#
#       * Checks only do value > threshold comparison. 
#          Both <, > and <> needs to be supported. At some point.
#
##################################################################


# Prevent creating files readable by others
umask 077
set +o noclobber                        # Allow clobbering of files ...
set -a                                  # Auto-export parameters set from now on
set +u                                  # Read unset parms without choking

# APIKEY generated in DataIku DSS for monitoring purposes
export APIKEY=input_apikey_string_here

# Base URL to DataIku DSS installation
export URL=https://dataiku_automation_node.yourdomain.com

# Proxy settings
export PROXY=http://your_proxy_server:proxy_port

# CURL timeout (seconds)
MAXTIME=45

# Init
D=
P=
ALL=false

#
DEBUG=false

Usage ()
        {
        echo ""
        echo "Check status of DataIku Datasets using the DSS REST API"
        echo ""
        echo "-key KEY      APIKEY to use when connecting"
        echo "-url URL      Base URL for REST API"
        echo "-proxy PROXY  Proxy server to use"
        echo ""
        echo "-d P:D,..     Comma separated list of PROJECT:DATASET names to check"
        echo "-p P,P,..     Comma spearated list of PROJECTS to check. Specify \"all\" to check datasets"
        echo "              in all projects that can run sceanrios. (See -def & -used)"
        echo "  -def        When -p is used, check all defined datasets in all selected projects"
        echo "  -used       When -p is used, parse projects scenarios to identify datasets being built"
        echo "              (This is the default behaviour)"
        echo ""
        echo "-m str,str,.. Comma separated list of metrics to check. Use format metric:w:c:age to check"
        echo "              w=warning threshold"
        echo "              c=warning threshold (for boolean use true/false as expected value. Critical if value doesn't match)."
        echo "              age=age of metric in days (critical generated if exceeded)"
        echo ""
        echo "Both -p -d can be specified at the same time"
        echo ""
        echo "Other options:"
        echo "-mt N         REST call timeout in seconds"
        echo "              (default:$MAXTIME)"
        echo "-v            Enable debug output"
        echo ""
        echo "Reporting (specify after -p and -def/-used):"
        echo "-lp           List projects visible to APIUSER"
        echo "-ld           List datasets visible to APIUSER. Requires -all or -p."
        echo "-lm           List metrics visible to APIUSER. Requires -all, -p or -d"
        echo "              only shows metrics with a timestamp and timestamp newer than specified with -ma"
        echo ""
        echo "Note:"
        echo "This check requires curl and jq to be installed"
        echo "Metrics without data (time = null) will be skipped"
        echo ""
        }

ParseArgs ()
        {
        if [ $# -eq 0 ] ; then
                Usage
                exit 0
        fi

        while [ $# -ne 0 ] ; do
        case $1 in
                -k)     shift
                        APIKEY=$1;;
                -url)   shift
                        URL=$1;;
                -proxy) shift
                        PROXY=$1;;
                -m)     shift
                        METRICS=$1;;
                -mt)    shift
                        MAXTIME=$1;;
                -d)     shift
                        D=$1;;
                -p)     shift
                        P=$1;;
                -v)     DEBUG=true;;
                -def)   ALL=true;;
                -used)  ALL=false;;
                -lp)    GetProjects
                        exit $?;;
                -ld)    ListDatasets
                        exit $?;;
                -lm)    ListDatasets | GetDatasetStatus
                        exit $?;;
                -h)     Usage
                        exit 0;;
                *)      echo "Invalid option : $1"
                        exit 1;;
        esac
        shift
        done
        }

Debug ()
        {
        [ "$DEBUG" = "true" ] && (>&2 echo "debug : $*")
        }

GetProjects()
        {
        # Ony return projects that can run scenarios
        Debug "Fetch projects :  curl -s --max-time $MAXTIME -x $PROXY -u $APIKEY: $URL/public/api/projects/"
        curl -s --max-time $MAXTIME -x $PROXY -u $APIKEY: $URL/public/api/projects/ | jq -r '.[] | select(.canRunScenarios == true) | .projectKey'
        return $?
        }

GetDatasets ()
        {
        # Return projectname:datasetname
        while read LINE ; do
                for P in $LINE ; do
                        if [ "$ALL" = "true" ] ; then
                                Debug "Fetching all datasets defined : curl -s --max-time $MAXTIME -x $PROXY -u $APIKEY: $URL/public/api/projects/$P/datasets/"
                                curl -s --max-time $MAXTIME -x $PROXY -u $APIKEY: $URL/public/api/projects/$P/datasets/ | jq  -r '.[] | .projectKey + ":" + .name '
                        else

                                Debug "Fetching active datasets : curl -s --max-time $MAXTIME -x $PROXY -u $APIKEY: $URL/public/api/projects/$P/scenarios/"
                                curl -s --max-time $MAXTIME -x $PROXY -u $APIKEY: $URL/public/api/projects/$P/scenarios/ \
                                        | jq '.[] | select(.active == true ) | .id' -cra  \
                                        | while read  S ; do
                                                Debug "Processing $S"
                                                curl -s --max-time $MAXTIME -x $PROXY -u $APIKEY: $URL/public/api/projects/$P/scenarios/$S \
                                                | jq '.projectKey+":"+.params.steps[].params.builds[]?.itemId' -cra
                                        done
                        fi
                done
        done
        return $?
        }

GetDatasetStatus()
        {
        while read LINE ; do
                for S in $LINE ; do
                        # Split S into 2 vars, using : as delimiter - project ${S%:*}, dataset ${S#*:}
                        P=${S%:*}
                        D=${S#*:}

                        # If someone formatted -d wrong (omitting the : char or the part after :), $D is either empty or equal to $S
                        [ "$D" = "" -o "$D" = "$S" ]  && return 1

                        # JQ :
                        #  Fetch the dataset info (last run) using curl
                        #  extract each metric for dataset (select last value for the metrics with arrays) and create new JSON file
                        Debug "Fetch dataset status : curl -s --max-time $MAXTIME -x $PROXY -u $APIKEY: $URL/public/api/projects/$P/datasets/$D/metrics/last/NP"
                        curl -s --max-time $MAXTIME -x $PROXY -u $APIKEY: $URL/public/api/projects/$P/datasets/$D/metrics/last/NP \
                        | jq '.metrics[]| {projectKey: env.P, dataset: env.D, metricType: .metric.metricType,  dataType: .metric.dataType, value: .lastValues[-1].value, partition: .lastValues[-1].partition, time: .lastValues[-1].computed}' -c
                done
        done
        }

ListDatasets ()
        {
        # Generate list of datasets to process based on input parameters
        # Output as string : project:dataset project:dataset project:dataset
        if [ "$P" = "all" ] ; then
                GetProjects      | GetDatasets
        else
                (
                echo "${P//,/ }"  | GetDatasets
                echo "${D//,/ }"
                )
        fi | grep -v -E "^$"
        }

CheckAge ()
        {
        # Check age of metric: (now-timestamp) < $A days
        [ "$A" == "" ] && return 0

        Debug "--> Checking Age : $A"
        jq 'select(.projectKey == env.P and .dataset == env.D and .metricType == env.M and .time != null and ((now-.time/1000)/60/60/24  > (env.A | tonumber)))
        | "FAILED :  Dataset "+.projectKey+"/"+.dataset+" ("+.partition+"), metric "+."metricType"+" (age > "+env.A+" days), "+(.time / 1000 | strftime("%Y-%m-%d %H:%M:%S"))+" (UTC)"' -rac $TMPFILE | grep -E "^FAILED"

        # If grep found "^FAILED" in output, assume a failed check
        [ $? -eq 0 ] && return 1
        return 0
        }

CheckCritical ()
        {
        # Check value of metric against threshold $C
        # If value is an integer, do as value > threshold comparion
        # If not - assume string comparison and raise an alert if value != threshold
        [ "$C" == "" ] && return 0

        Debug "--> Checking Crit : $C"
        jq 'select(.projectKey == env.P and .dataset == env.D and .metricType == env.M and .time != null )
        | if (.dataType == "BIGINT")
        then (select ((.value | tonumber) > (env.C | tonumber))
                | "FAILED")
        else (select(.value != env.C )
                | "FAILED")
        end+" :  Dataset "+.projectKey+"/"+.dataset+" ("+.partition+"), metric "+."metricType"+" ("+.value+" > "+env.C+"), "+(.time / 1000 | strftime("%Y-%m-%d %H:%M:%S"))+" (UTC)"' -rac $TMPFILE | grep -E "^FAILED"

        # If grep found "^FAILED" in output, assume a failed check
        [ $? -eq 0 ] && return 1
        return 0
        }

CheckWarning ()
        {
        # Check value of metric against threshold $W
        # If value is an integer, do as value > threshold comparion
        # If not - assume string comparison and raise an alert if value != threshold
        [ "$W" == "" ] && return 0

        Debug "--> Checking Warn : $W"
        jq 'select(.projectKey == env.P and .dataset == env.D and .metricType == env.M and .time != null)
        | if (.dataType == "BIGINT")
        then (select ((.value | tonumber) > (env.W | tonumber))
                | "WARNING")
        else (select(.value != env.W )
                | "WARNING")
        end+" : Dataset "+.projectKey+"/"+.dataset+" ("+.partition+"), metric "+."metricType"+" ("+.value+" > "+env.C+"), "+(.time / 1000 | strftime("%Y-%m-%d %H:%M:%S"))+" (UTC)"' -rac $TMPFILE | grep -E "^WARNING"
#       end+" : Dataset "+.projectKey+"/"+.dataset+" ("+.partition+"), metric "+."metricType"+" ("+(.time / 1000 | strftime("%Y-%m-%d %H:%M:%S"))+" UTC)"' -rac $TMPFILE | grep -E "^WARNING"

        # If grep found "^WARNING" in output, assume a failed check
        [ $? -eq 0 ] && return 1
        return 0
        }

OKMessage ()
        {
        Debug "--> Seems OK"
        jq 'select(.projectKey == env.P and .dataset == env.D and .metricType == env.M and .time != null)
        | "OK : Dataset "+.projectKey+"/"+.dataset+" ("+.partition+"), metric "+."metricType"+" ("+(.time / 1000 | strftime("%Y-%m-%d %H:%M:%S"))+" UTC)"' -rac $TMPFILE
        }

# ------------------------
#
#      M   A   I   N
#
# ------------------------

ParseArgs "$@"

# -------------------------
# ... or check status
# -------------------------

RESFILE=/tmp/$$.$RANDOM.res
TMPFILE=/tmp/$$.$RANDOM

# Send string with "project:dataset project:dataset ..." to processsing
for DS in $(ListDatasets) ; do
        Debug "PARSING DATASET : $DS"

        # Get project:dataset info from REST API and store for parsing
        echo $DS | GetDatasetStatus >| $TMPFILE

        [ "$DEBUG" = "true" ] && (cat $TMPFILE | sed 's/^/debug : /' )

        P=${DS%:*}
        D=${DS#*:}

        # Check each specified metric check for the current dataset (DS)
        # Format : METRIC_NAME:WARNING_THRESHIOLD:CRITICAL_THRESHOLD:METRIC_AGE_THRESHOLD
        for MET in ${METRICS//,/ } ; do
                # Split metric check definiton into variables and process
                echo $MET |  while IFS=: read M W C A ; do
                        Debug "->  METRIC $MET"
                        # Skip if no checks specified
                        [ "$W$C$A" = "" ] && break

                        # Check each metric - break out of loop if an alert is found
                        CheckAge        || break
                        CheckCritical   || break
                        CheckWarning    || break
                        OKMessage
                done
        done
done | tee -a $RESFILE

EC=0
# Check for WARNING to set exitcode
grep -q -E "^WARNING" $RESFILE
if [ $? -eq 0 ] ; then
        EC=1
fi

# Check for FAILED to set exitcode
grep -q -E "^FAILED" $RESFILE
if [ $? -eq 0 ] ; then
        EC=2
fi

[ -s $RESFILE ] || echo "No matching metrics found"


rm -f $TMPFILE $RESFILE
exit $EC
