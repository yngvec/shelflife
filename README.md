# **README** #


## **What's this?** ##

This repository stores a collection of code and information about topics I find interesting (or found interesting at some point in time).

Some of it may be of use to others.

## **Folders** ##

| | |
| -------- | --------|
| **nagios**   | **_Contains checks for Nagios_** |
| check_dataiku_dataset    |  _Check for DataIku DSS Datasets using curl and jq_   |	 
| check_dataiku_scenario   |  _Check for DataIku DSS Sceanrios using curl and jq_  |
| check_goldengate         |  _Check for Oracle Goldengate using curl and jq_      |
| check_goldengate_classic |  _Check for Oracle Goldengate Classic using ggsci_    |  
|  |  |
| **telegraf** | **_Contains plugins for telegraf. Once useful for InfluxDB & Grafana, ... now mostly broken_**  |
| fibaro2influx            |  _Plugin to collect info from Fibaro HC2 using curl and jq_  |
| tibber2influx            |  _Plugin to collect info from Tibber API using curl and jq_  |  
| met2influx            |  _Plugin to collect info from The Norwegian Meteorological Institute weather API using curl and xmllint_  |  
|  |  |
  